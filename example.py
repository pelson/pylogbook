import pylogbook

import urllib3
urllib3.disable_warnings()


client = pylogbook.Client(server_url=pylogbook.NamedServer.TEST)
TESTS_LOGBOOK = client.get_activities()['LOGBOOK_TESTS_Long_Name Testing']

logbook = pylogbook.ActivitiesClient(
    activities=TESTS_LOGBOOK,
    client=client,
)

event = logbook.add_event("Testing from pylogbook v3.")
event.attach_file('example/snake.png')
event.attach_content('Attaching some content', name='my-file.txt')


print(f"Logbook posted. See it at: https://cs-ccr-logdev.cern.ch/elogbook-server#/logbook?logbookId={TESTS_LOGBOOK.activity_id}")
