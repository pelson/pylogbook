import enum


class NamedActivity(enum.Enum):
    """Named logbook which may be passed as Activities to the Client.

    For an exhaustive list of activities see :attr:`pylogbook.Client.get_activities`.

    """
    CLEAR = "CLEAR"
    ELENA = "ELENA"
    LEIR = "LEIR"
    LINAC3 = "LINAC_3"
    LINAC4 = "LINAC_4"
    LHC = "LHC"
    PS = "PS"
    PSB = "PSB"
    SPS = "SPS"
