"""
The high-level clients which interact with the low-level
client, :class:`pylogbook._low_level_client.LogbookServiceClient`, and produce
high-level objects from :mod:`pylogbook.models`.

"""
import datetime
import logging
from typing import Optional, Union, Dict, List, Tuple

from . import auth as _auth
from .exceptions import LogbookError
from ._activity_names import NamedActivity
from ._low_level_client import LogbookServiceClient
from ._server_names import NamedServer
from .models import Activity, Event, ActivitiesType, PaginatedEvents, PaginatedTags, Tag, TagsType


logger = logging.getLogger(__name__)


class Client:
    """A high-level client to a logbook service.

    """
    def __init__(
            self,
            server_url: Union[str, NamedServer] = NamedServer.PRO,
            rbac_token: Optional[_auth.TokenType] = None,
            auth: Optional[_auth.AuthManager] = None,
    ) -> None:
        """A high-level interface to the given logbook-service.

        Parameters
        ----------
        server_url : str, :class:`~pylogbook.NamedServer`
            The server to interact with.

        rbac_token : str, PyRBAC Token instance
            The RBAC token to use when authenticating against the logbook server.
            Mutually exclusive with the ``auth`` keyword argument.

        auth : :class:`~pylogbook.auth.AuthManager` instance
            The authentication scheme to use when against the logbook server.
            Mutually exclusive with the ``rbac_token`` keyword argument.

        """
        #: The low-level client which can communicate with the underlying logbook service.
        self._service_client = LogbookServiceClient(
            server_url=server_url,
            rbac_token=rbac_token,
            auth=auth,
        )

    @property
    def rbac_b64_token(self) -> str:
        """
        Get and set the base 64 encoded RBAC token.

        :getter: Return a string containing the base 64 encoded token used
            when authenticating with the logbook service.
        :setter: Set the RBAC token to use for authentication. Can be of type
            string (base 64 encoded), or a PyRBAC token instance (or any instance
            with with a ``token.b64_encoded()`` method).

        """
        return self._service_client.rbac_b64_token

    @rbac_b64_token.setter
    def rbac_b64_token(self, token: _auth.TokenType):
        self._service_client.rbac_b64_token = token

    @property
    def auth(self) -> _auth.AuthManager:
        """Return the Authentication manager for this client"""
        return self._service_client.auth

    def get_activities(self) -> Dict[str, Activity]:
        """
        Return a mapping of activity name to :class:`~pylogbook.models.Activity`
        instances found on the server.

        """
        resp = self._service_client.get_logbooks()
        LogbookError.raise_for_status(resp)
        result = {}
        for logbook_data in resp.json():
            activity_data = logbook_data['activity']
            activity = Activity.build_from_json_response(self._service_client, activity_data)
            result[activity.name] = activity
        return result

    def add_event(
            self,
            activities: ActivitiesType,
            message: str,
            tags: TagsType = (),
    ) -> Event:
        """
        Post an event to the given activities.

        Parameters
        ----------
        activities:
            The activities to which the event should be posted.
        message:
            The message to post with the event.
        tags:
            The tags to associate with the event

        """
        resp = self._service_client.post_event(
            message,
            activities=self._convert_to_activities_ids(activities),
            tags=self._convert_to_tags_ids(tags),
        )
        LogbookError.raise_for_status(resp)
        event = Event.build_from_json_response(self._service_client, resp.json())
        return event

    def _convert_to_tags_ids(
            self, tags: TagsType
    ) -> Tuple[int, ...]:
        return tuple(
            tag.tag_id for tag in self._convert_to_tags(tags)
        )

    def _convert_to_tags(
            self, tags: TagsType
    ) -> Tuple[Tag, ...]:
        normalised_tags: List[Tag] = []

        # Lazily get the activities.
        _prepared_tags = None

        def prepared_tags() -> Dict[str, Tag]:
            # Returns a tags_by_name dictionary.
            # Will only fetch the data once per _convert_to_tags call,
            # on the first call of this func.
            nonlocal _prepared_tags
            if _prepared_tags is None:
                tags_by_name = {tag_.name: tag_ for tag_ in self.get_tags()}
                _prepared_tags = tags_by_name
            return _prepared_tags

        def get_tag_by_name(tag_name: str) -> Tag:
            all_tags_by_name = prepared_tags()
            if tag_name not in all_tags_by_name:
                raise ValueError(
                    f'No tag named "{tag_name}" was found on the server.'
                )
            return all_tags_by_name[tag_name]

        if isinstance(tags, (str, Tag)):
            tags = [tags]

        for tag in tags:
            if isinstance(tag, str):
                normalised_tags.append(get_tag_by_name(tag))
            elif isinstance(tag, Tag):
                normalised_tags.append(tag)
            else:
                raise TypeError(f"Unexpected type {type(tag)} provided")
        return tuple(normalised_tags)

    def _convert_to_activities_ids(
            self, activities: Optional[ActivitiesType]
    ) -> Optional[Tuple[int, ...]]:
        if activities is not None:
            return tuple(
                activity.activity_id for activity in self._convert_to_activities(activities)
            )

    def _convert_to_activities(
            self, activities: ActivitiesType
    ) -> Tuple[Activity, ...]:
        normalised_activities: List[Activity] = []

        # Lazily get the activities.
        _prepared_activities = None

        def prepared_activities() -> Tuple[Dict[str, Activity], Dict[int, Activity]]:
            # Returns a tuple of (activity_by_name, activities_by_id).
            # Will only fetch the data once, on the first call of this func.
            nonlocal _prepared_activities
            if _prepared_activities is None:
                activities_by_name = self.get_activities()
                activities_by_id = {
                    activity_.activity_id: activity_
                    for activity_ in activities_by_name.values()
                }
                _prepared_activities = activities_by_name, activities_by_id
            return _prepared_activities

        def get_activity_by_name(activity_name: str) -> Activity:
            all_activities_by_name = prepared_activities()[0]
            if activity_name not in all_activities_by_name:
                raise ValueError(
                    f'No activity named "{activity_name}" was found on the server.'
                )
            return all_activities_by_name[activity_name]

        def get_activity_by_id(activity_id: int) -> Activity:
            all_activities_by_id = prepared_activities()[1]
            if activity_id not in all_activities_by_id:
                raise ValueError(
                    f'No activity with id {activity_id} was found on the server.'
                )
            return all_activities_by_id[activity_id]

        if not isinstance(activities, (list, tuple)):
            activities = [activities]

        for activity in activities:
            if isinstance(activity, str):
                normalised_activities.append(get_activity_by_name(activity))
            elif isinstance(activity, NamedActivity):
                normalised_activities.append(get_activity_by_name(activity.name))
            elif isinstance(activity, int):
                normalised_activities.append(get_activity_by_id(activity))
            elif isinstance(activity, Activity):
                normalised_activities.append(activity)
            else:
                raise TypeError(f"Unexpected type {type(activity)} provided")
        return tuple(normalised_activities)

    def get_event(self, event_id: Union[int, Event]) -> Event:
        """
        Fetch an already existing event from the server.

        """
        if isinstance(event_id, Event):
            event_id = event_id.event_id
        resp = self._service_client.get_event(event_id)
        LogbookError.raise_for_status(resp)
        event = Event.build_from_json_response(self._service_client, resp.json())
        return event

    def get_events(
            self,
            activities: Optional[ActivitiesType] = None,
            from_date: Optional[datetime.datetime] = None,
            to_date: Optional[datetime.datetime] = None,
    ) -> PaginatedEvents:
        """
        Return an iterable of :class:`Event` instances which match the given conditions

        Notes
        -----
        As a result of the storage used in the underlying eLogbook service, even
        paginated requests can suffer from poor performance. It is therefore
        recommended to try to minimise the number of results in the overall query.
        For example, simply setting a ``from_date`` to the earliest date required
        is likely to result in better query performance. For more details see
        https://gitlab.cern.ch/scripting-tools/pylogbook/-/issues/6.

        """
        query = {
            "activities": self._convert_to_activities_ids(activities),
            "from_date": from_date,
            "to_date": to_date,
        }
        resp = self._service_client.get_events(count_only=True, **query)
        LogbookError.raise_for_status(resp)
        query['count_only'] = False
        events = PaginatedEvents(resp.json()['answerAsCount'], query)
        events._service_client = self._service_client
        return events

    def get_tags(self) -> PaginatedTags:
        """
        Return an iterable of :class:`~pylogbook.models.Tag` instances.

        """
        resp = self._service_client.get_tags(count_only=True)
        LogbookError.raise_for_status(resp)
        tags = PaginatedTags(resp.json()['answerAsCount'])
        tags._service_client = self._service_client
        return tags

    def get_tag(self, *, name: str) -> Tag:
        """
        Return a :class:`~pylogbook.models.Tag` instance matching the given conditions.

        Parameters
        ----------

        name:
            The name of the tag to match.

        Notes
        -----
        Because there is currently no server-side filtering, and it is
        therefore being done client-side, if you need to call this method
        multiple times, it will be more efficient to extract all the tags and
        filter that yourself. For example::

            tags = list(cli.get_tags())
            [tag] = [tag for tag in tags if tag.name == 'my-tag']

        """
        # Note: We can't currently filter the logbook GET/tags response, so we
        # have to do it client-side. This may change in the future.
        for tag in self.get_tags():
            if tag.name == name:
                return tag
        else:
            raise ValueError(f'The tag named "{name}" was not found')


class ActivitiesClient:
    """Represents one or more activities, to which events can be posted."""
    def __init__(self,
                 activities: ActivitiesType,
                 client: Optional[Client] = None,
                 ):
        """An ActivitiesClient instance holds a fixed set of activities (which
        are defined at instantiation).

        """
        self._client = client or Client()
        self._activities: Tuple[Activity, ...] = self._client._convert_to_activities(activities)

    @property
    def activities(self) -> Tuple[Activity, ...]:
        """
        The activities that this client represents.

        It is possible to set the activities using the ``activities`` property
        setter.

        """
        return self._activities

    @activities.setter
    def activities(self, activities: ActivitiesType):
        self._activities = self._client._convert_to_activities(activities)

    def add_event(
            self,
            message: str,
            *,
            tags: TagsType = (),
    ) -> Event:
        """
        Post an event which is associated with this ActivitiesClient's activities.

        See :meth:`Client.add_event` for argument details.

        """
        event = self._client.add_event(
            activities=self._activities,
            message=message,
            tags=tags,
        )
        return event

    def get_events(self,
            from_date: Optional[datetime.datetime] = None,
            to_date: Optional[datetime.datetime] = None,
    ) -> PaginatedEvents:
        """
        Return an iterator of events found on these activities, which match the given conditions

        See also the note in :meth:`Client.get_events`.

        """
        return self._client.get_events(
            self._activities, from_date=from_date, to_date=to_date,
        )
