"""
High-level data models to represent key logbook objects.

It is unlikely that you should need to construct one of these objects directly.
Instead, the expected behaviour is to use a :class:`pylogbook.Client` or
:class:`pylogbook.ActivitiesClient` instance to build a data model based on data
from the logbook service.

"""
import dataclasses
import datetime
import logging
import math

from typing import List, Union, Generator, Tuple
import typing

# Note: no imports from ._client should be here - the client builds on top of the
# datamodels in this module.
from ._activity_names import NamedActivity as _NamedActivity
from ._attachment_builder import AttachmentBuilder as _AttachmentBuilder
from . import exceptions
from ._low_level_client import LogbookServiceClient as _LLClient


logger = logging.getLogger(__name__)

TagType = Union[str, "Tag"]
TagsType = Union[TagType, typing.Iterable[TagType]]
ActivityType = Union[int, str, "Activity", _NamedActivity]
ActivitiesType = Union[ActivityType, List[ActivityType], Tuple[ActivityType, ...]]


@dataclasses.dataclass
class Attachment:
    """
    Represents an Attachment on the logbook server.

    If you want to build an attachment to be sent to the server,
    see :meth:`Event.attach_file` and :meth:`Event.attach_content`.

    """
    attachment_id: int
    filename: str
    creator: str

    #: The client upon which further actions can be carried out.
    _service_client: _LLClient = dataclasses.field(repr=False, init=False)

    #: Other parts of the Attachment data returned by the service which
    #: haven't been implemented yet.
    _extra: dict = dataclasses.field(default_factory=dict, repr=False, init=False)

    @classmethod
    def build_from_json_response(cls, service_client: _LLClient, data: dict) -> "Attachment":
        """
        Build an Attachment instance from an elogbook-server response payload.

        """
        data = data.copy()
        kwargs = {
            'attachment_id': data.pop('id'),
            'filename': data.pop('fileName'),
            'creator': data.pop('creator'),
        }
        attachment = cls(**kwargs)
        attachment._service_client = service_client
        attachment._extra = data
        return attachment


Attachment.__init__.__doc__ = """
    It is not recommended to build an Attachment directly.

    If you want to build an attachment to be sent to the server,
    see :meth:`Event.attach_file` and :meth:`Event.attach_content`..

"""


@dataclasses.dataclass
class Tag:
    """
    Represents a Tag on the logbook server.

    There is currently no way to create a Tag on the server, please contact
    eLogbook admin for this to be done manually.

    You can get a tag with :meth:`pylogbook.Client.get_tags`.

    """
    #: The ID of the tag on the logbook server.
    tag_id: int

    #: The name of the tag.
    name: str

    #: The client upon which further actions can be carried out.
    _service_client: _LLClient = dataclasses.field(repr=False, init=False)

    #: Other parts of the Tag data returned by the service which
    #: haven't been implemented yet.
    _extra: dict = dataclasses.field(default_factory=dict, repr=False, init=False)

    @classmethod
    def build_from_json_response(cls, service_client: _LLClient, data: dict) -> "Tag":
        """Build a Tag instance from an elogbook-server response payload.

        """
        data = data.copy()
        kwargs = {
            'tag_id': data.pop('id'),
            'name': data.pop('name'),
        }
        data.pop('url')
        attachment = cls(**kwargs)
        attachment._service_client = service_client
        attachment._extra = data
        return attachment


Tag.__init__.__doc__ = """
    It is not recommended to build a Tag directly.

"""


@dataclasses.dataclass
class PaginatedTags:
    """
    An iterator of :class:`Tag` instances fetched lazily (paginated) from the logbook server.

    """
    #: The number of :class:`Tags` instances are in the result.
    count: int
    page_size: int = 1000

    #: The client upon which further actions can be carried out.
    _service_client: _LLClient = dataclasses.field(repr=False, init=False)

    def get_page(self, page_number: int = 0) -> Generator[Tag, None, None]:
        """
        Get a specific page of tags. Page numbers start at 0.
        """
        resp = self._service_client.get_tags(
            count_only=False,
            records_limit=self.page_size,
            start_record=page_number * self.page_size,
        )
        exceptions.LogbookError.raise_for_status(resp)
        for tag_json in resp.json()['answerAsList']:
            yield Tag.build_from_json_response(self._service_client, tag_json)

    def n_pages(self) -> int:
        n_pages = math.ceil(self.count / float(self.page_size))
        return n_pages

    def __iter__(self) -> typing.Iterator[Tag]:
        for page in range(self.n_pages()):
            yield from self.get_page(page)


@dataclasses.dataclass
class Event:
    """
    Represents an Event on the logbook server.

    If you want to build an event to be sent to the server,
    see :class:`pylogbook.Client.add_event` and :class:`pylogbook.ActivitiesClient.add_event`.
    If you want to access an existing event by
    ID, see :class:`pylogbook.Client.get_event`.

    """
    event_id: int
    comment: str
    creator: str
    date: datetime.datetime
    attachments: List[Attachment]
    tags: List[Tag]

    #: The client upon which further actions can be carried out.
    _service_client: _LLClient = dataclasses.field(repr=False, init=False)

    #: Other parts of the Event data returned by the service which
    #: haven't been implemented yet.
    _extra: dict = dataclasses.field(default_factory=dict, repr=False, init=False)

    @classmethod
    def build_from_json_response(cls, service_client: _LLClient, data: dict) -> "Event":
        """
        Build an Event instance from an elogbook-server response payload.

        """
        data = data.copy()
        kwargs = {
            'event_id': data.pop('id'),
            'comment': data.pop('comment'),
            'creator': data.pop('creator'),
            'date': datetime.datetime.fromisoformat(data.pop('dateOfEvent')),
            'attachments': [
                Attachment.build_from_json_response(service_client, attachment)
                for attachment in data.pop('attachments') or []
            ],
            'tags': [
                Tag.build_from_json_response(service_client, tag)
                for tag in data.pop('tags', []) or []
            ],
        }
        event = cls(**kwargs)
        event._service_client = service_client
        event._extra = data
        return event

    def _add_attachment(self, attachment: _AttachmentBuilder) -> Attachment:
        """
        Add an attachment, such as an image or a file, to the event.

        """
        resp = self._service_client.add_attachment(self.event_id, attachment)
        exceptions.LogbookError.raise_for_status(resp)
        attachment = Attachment.build_from_json_response(self._service_client, resp.json())
        self.attachments.append(attachment)
        return attachment

    def attach_file(self, filename: str) -> Attachment:
        """Build an AttachmentBuilder by filename."""
        return self._add_attachment(_AttachmentBuilder.from_file(filename))

    def attach_content(
            self,
            contents: Union[str, bytes],
            mime_type: str = "text/plain",
            name: str = "file",
    ) -> Attachment:
        """
        Attach file-data to an event.

        :param contents: data to be attached
        :param mime_type: mime type of the resource
        :param name: name of the resource (default=file)

        """
        if isinstance(contents, str):
            attachment = _AttachmentBuilder.from_string(
                contents, mime_type=mime_type, name=name,
            )
        else:
            attachment = _AttachmentBuilder.from_bytes(
                contents, mime_type=mime_type, name=name,
            )
        return self._add_attachment(attachment)

    # TODO: Add delete once it is properly implemented on the server side.


Event.__init__.__doc__ = """
    It is not recommended to build an Event directly.

    If you want to build an event to be sent to the server,
    see :class:`pylogbook.Client.add_event` and :class:`pylogbook.ActivitiesClient.add_event`.
    If you want to access an existing event by
    ID, see :class:`pylogbook.Client.get_event`.

"""


@dataclasses.dataclass
class PaginatedEvents:
    """
    An iterator of :class:`Event` instances fetched lazily (paginated) from the logbook server.

    Since this is an iterator of :class:`Event`, you can loop over *all* events::

        for event in paginaged_events:
            event.event_id
            break  # Note that breaking early will avoid fetching all the data from the server.

    """
    #: The number of :class:`Activity` instances are in the result.
    count: int
    query_arguments: dict
    page_size: int = 25

    #: The client upon which further actions can be carried out.
    _service_client: _LLClient = dataclasses.field(repr=False, init=False)

    def get_page(self, page_number: int = 0) -> Generator[Event, None, None]:
        """
        Get a specific page of events. Page numbers start at 0.
        """
        query = self.query_arguments.copy()
        resp = self._service_client.get_events(
            records_limit=self.page_size,
            start_record=page_number * self.page_size,
            **query,
        )
        exceptions.LogbookError.raise_for_status(resp)
        for event_json in resp.json()['answerAsList']:
            yield Event.build_from_json_response(self._service_client, event_json)

    def n_pages(self) -> int:
        n_pages = math.ceil(self.count / float(self.page_size))
        return n_pages

    def __iter__(self) -> typing.Iterator[Event]:
        for page in range(self.n_pages()):
            yield from self.get_page(page)


@dataclasses.dataclass
class Activity:
    """
    Represents an Activity on the logbook server.

    To get an Activity instance, use the :meth:`pylogbook.Client.get_activities` method.

    """
    activity_id: int
    name: str

    #: The client upon which further actions can be carried out.
    _service_client: _LLClient = dataclasses.field(repr=False, init=False)

    #: Other parts of the Activity data returned by the service which
    #: haven't been implemented yet.
    _extra: dict = dataclasses.field(default_factory=dict, repr=False, init=False)

    @classmethod
    def build_from_json_response(cls, service_client: _LLClient, data: dict) -> "Activity":
        """Build an Activity instance from an elogbook-server response payload.

        """
        data = data.copy()
        kwargs = {
            'activity_id': data.pop('id'),
            'name': data.pop('name'),
        }
        attachment = cls(**kwargs)
        attachment._service_client = service_client
        attachment._extra = data
        return attachment


Activity.__init__.__doc__ = """
    It is not recommended to build an Activity directly.

    See also :meth:`pylogbook.Client.get_activities`.

"""
