import dataclasses
import mimetypes
from pathlib import PurePath
from typing import Union
import typing


AttachmentBuilderType = typing.TypeVar('AttachmentBuilderType', bound='AttachmentBuilder')


@dataclasses.dataclass
class AttachmentBuilder:
    """A class to represent an attachment to be submitted to the logbook service

    To create an AttachmentBuilder one of :meth:`AttachmentBuilder.from_file`
    or :meth:`AttachmentBuilder.from_string` is recommended.

    """
    contents: Union[str, bytes]
    short_name: str
    mime_type: str

    @classmethod
    def from_file(cls: typing.Type[AttachmentBuilderType], filename: str) -> AttachmentBuilderType:
        """
        Build an AttachmentBuilder by filename.

        :param filename: name of the file to be attached, last part will be used as the name of the resource
        :return: AttachmentBuilder object created from the file
        """
        with open(filename, "rb") as f:
            contents = f.read()
            short_name = PurePath(filename).name
            mime_type = mimetypes.guess_type(short_name)[0]
            return cls(contents, short_name, mime_type)

    @classmethod
    def from_bytes(
            cls: typing.Type[AttachmentBuilderType],
            contents: bytes,
            mime_type: str,
            name="file"
    ) -> AttachmentBuilderType:
        """
        Build an AttachmentBuilder by file contents.

        :param contents: data to be attached
        :param mime_type: mime type of the resource
        :param name: name of the resource (default=file)
        :return: AttachmentBuilder object created from bytes
        """
        return cls(contents, name, mime_type)

    @classmethod
    def from_string(
            cls: typing.Type[AttachmentBuilderType],
            contents: str,
            mime_type: str = "text/plain",
            name="file"
    ) -> AttachmentBuilderType:
        """
        Build an AttachmentBuilder by file contents.

        :param contents: data to be attached
        :param mime_type: mime type of the resource (default="text/plain")
        :param name: name of the resource (default=file)
        :return: AttachmentBuilder object created from a string
        """
        return cls(contents, name, mime_type)


AttachmentBuilder.__init__.__doc__ = """
    It is not recommended to build an AttachmentBuilder directly.

    See :meth:`~AttachmentBuilder.from_file` or :meth:`~AttachmentBuilder.from_string`
    instead.

"""
