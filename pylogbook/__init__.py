"""An interface to the logbook service.
"""

from ._version import version as __version__

from ._activity_names import NamedActivity
from ._client import ActivitiesClient, Client
from ._server_names import NamedServer

# pylogbook.exceptions is a public API which is automatically imported.
from . import exceptions  # noqa


ActivitiesClient.__module__ = __name__
Client.__module__ = __name__
NamedActivity.__module__ = __name__
NamedServer.__module__ = __name__
