import base64
import os
import typing
import typing_extensions
import warnings


__all__ = ["AuthManager", "RbacAuthManager", "BearerToken"]


@typing_extensions.runtime_checkable
class _PyRBACToken(typing_extensions.Protocol):
    # The key PyRBAC token protocol which allows us to get the token from the
    # Token object. This weak duck-type protocol is not considered "public & supported",
    # and can be replaced as soon as the API in
    # https://issues.cern.ch/browse/RBAC-966 is released (potentially breaking
    # for those who rely on this weak protocol, or those who don't upgrade
    # pyrbac at the same time as upgrading pylogbook).
    def get_encoded(self) -> bytes: ...

    # We add a extra certainty that this is an PyRBAC token-like thing by
    # including another method. Once b64_encoded exists, instead of
    # get_encoded + manual encoding we can remove this extra constraint, and let
    # this protocol truly quack like a duck.
    def get_user_name(self) -> str: ...


TokenType = typing.Union[str, _PyRBACToken]


class AuthManager:
    """The base authentication class"""

    def http_headers(self) -> dict:
        """
        The http headers that should be set in order to authenticate
        to the service using this authentication scheme.

        """
        return {}


class RbacAuthManager(AuthManager):
    """
    The RBAC authentication scheme.

    Note: The token may be an empty string (``''``) to allow deferred
    setting of the token. Note that such a value will not result in an error
    when a token is required for authentication, and it is therefore likely that
    authentication issues (:class:`pylogbook.exceptions.AuthenticationError`)
    will be the result of any API requests.

    """
    def __init__(self, b64_token: typing.Optional[TokenType] = None):
        self._b64_token: str = ''
        self.b64_token = b64_token

    @property
    def b64_token(self) -> str:
        """
        Get the base 64 encoded RBAC token.

        """
        return self._b64_token

    @b64_token.setter
    def b64_token(self, token: typing.Optional[TokenType]) -> None:
        self._b64_token = self.as_b64_token(token)

    def as_b64_token(self, token: typing.Optional[TokenType]) -> str:
        """
        Return the given "token" as a stringified base 64 encoded token.

        Parameters
        ----------

        token:
            The token to set for this authentication manager.

            Note that if None is set then the token will be taken from the
            :meth:`b64_token_from_env` method, which may raise if the environment
            variable is not set.

        """
        if isinstance(token, str):
            return token
        elif isinstance(token, _PyRBACToken):
            return base64.b64encode(token.get_encoded()).decode()
        elif token is None:
            return self.b64_token_from_env()
        else:
            raise ValueError(f"Unsupported token type {type(token)}")

    def b64_token_from_env(self) -> str:
        """Get the RBAC token from the ``RBAC_TOKEN_SERIALIZED``, or raise."""
        token = os.environ.get("RBAC_TOKEN_SERIALIZED", None)
        if not token:
            raise ValueError(
                "Unable to get the RBAC token from the RBAC_TOKEN_SERIALIZED "
                "environment variable."
            )
        # Remove any quotes or trailing semi-colons, which has been seen when
        # ``eval $(/acc/local/L866/cmw/cmw-tools/PRO/bin/RbacAuthenticate -e)``
        return token.strip('\'').rstrip('\';')

    def http_headers(self) -> dict:
        token = self.b64_token
        return {"Authorization": "RBAC " + token}


class BearerToken(AuthManager):
    """Use Bearer token authentication"""
    def __init__(self, token: str):
        self.token = token

    def http_headers(self) -> dict:
        return {"Authorization": f"Bearer {self.token}"}
