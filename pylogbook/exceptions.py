"""Specific exceptions raised by pylogbook.

"""
import requests


class LogbookError(Exception):
    """Raised when any issues encountered during interaction with the LogbookServiceClient
    server.

    """
    def __init__(self, message, response):
        super().__init__(message)
        self._response = response

    @classmethod
    def raise_for_status(cls, response: requests.Response):
        if response.status_code == 401:
            raise AuthenticationError(
                f'Logbook service not authenticated',
                response,
            )
        elif 400 <= response.status_code < 600:
            raise LogbookError(
                f"Logbook service error (code { response.status_code }) encountered",
                response,
            )


class AuthenticationError(LogbookError):
    """
    Raised when a 401 error is returned by the eLogbook service.

    """
    pass
