pylogbook
=========

Introduction
------------

A Python interface to the BE-OP Logbook service


Installation
------------

Using the `acc-py Python package index
<https://wikis.cern.ch/pages/viewpage.action?pageId=145493385>`_
``pylogbook`` can be pip installed with::

   pip install pylogbook


Documentation contents
----------------------

.. toctree::
    :maxdepth: 1
    :hidden:

    self

.. toctree::
    :caption: pylogbook
    :maxdepth: 1

    usage

.. toctree::
    :caption: Reference docs
    :maxdepth: 1

    api
    genindex

