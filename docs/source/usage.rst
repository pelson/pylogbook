.. _usage:

Usage
=====

The very first step is to import pylogbook::

    import pylogbook

From here, we can construct a high-level client to the logbook service::

    cli = pylogbook.Client()

By default, this client will talk to the production logbook service, and use the
``RBAC_TOKEN_SERIALIZED`` environment variable to authenticate against the service.

If you'd like to specify a :class:`~pylogbook.NamedServer` or RBAC token explicitly
you can do so when constructing the :class:`~pylogbook.Client`. For example::

    cli = pylogbook.Client(
        rbac_token=my_rbac_token,
        server=pylogbook.NamedServer.TEST,
    )

With a :class:`pylogbook.Client` instance we can do things such as
:meth:`~pylogbook.Client.get_activities`, :meth:`~pylogbook.Client.add_event` and
:meth:`~pylogbook.Client.get_event`. For example, to create an event on the logbook server::

    event = cli.add_event("My event message", activities=pylogbook.NamedActivities.LHC)

If you find that you are always posting to the same activities (logbooks in old terms), there is a dedicated
:class:`pylogbook.ActivitiesClient` whose sole responsibility is to store the
activities that should be passed on to the client. For example::

    logbook = pylogbook.ActivitiesClient(pylogbook.NamedActivities.LHC)

    logbook.add_event("Event 1 on the LHC logbook")
    logbook.add_event("Event 2 on the LHC logbook")

You can also pass through a :class:`pylogbook.Client` instance when constructing the
:class:`pylogbook.ActivitiesClient` in order to control things such as the RBAC
token and the targeted logbook server.


Authentication
==============

You must have a valid authentication to be able to interact with the logbook service.
The current authentication providers include RBAC and OIDC.
In addition, your account must be enabled by the logbook service owner.
See https://wikis.cern.ch/display/EL/Logbook for details of the logbook service.

For RBAC Authentication
-----------------------

Once registered with the logbook service you will need an RBAC token.
The mode by which a token is attained depends on the circumstance:

 * in an operational context it is likely that you can logon by location
 * in a non-operational service context you may want to get a token through explicit (username & password) logon
 * in a development context, where repeatedly entering a username and password becomes laborious,
   you may want to set the ``RBAC_TOKEN_SERIALIZED`` environment variable

In any case, a library such as :mod:`pyrbac` may be used, either via
:class:`pyrbac.LoginServiceBuilder` or :class:`pyrbac.AuthenticationClient`.
Alternatively, you can also set the ``RBAC_TOKEN_SERIALIZED`` environment variable with
a tool such as ``rbac-authenticate`` from the managed OS (if running before starting Python)::

    eval $(rbac-authenticate -e)

For non-RBAC authentication
---------------------------

It is possible to use methods other than RBAC for authentication. These are much less
frequently required, but can be employed by specifying a custom ``auth`` argument
to the client's constructor. For OIDC you can setup a client with a specific bearer
token using the :class:`pyrbac.auth.BearerToken` class.


Logbook models
==============

Event and Attachment objects
----------------------------

:mod:`pylogbook` has a number of data models which give a rich interface to the
logbook service. We've already seen one of these models: the :class:`~pylogbook.models.Event` class
was constructed for us when we called the :meth:`pylogbook.Client.add_event` method::

    event = logbook.add_event("My event message")

This object has a reference to the client from which it was built, and can therefore
continue to interact with the logbook service. For example, we can add an attachment to the
event with the :meth:`~pylogbook.models.Event.attach_file` method::

    event.attach_file('my-attachment.png')

Alternatively, if you want to attach bytes instead of a file, you can do so with the
:meth:`~pylogbook.models.Event.attach_content` method::

    event.attach_content("A log message, attached as a file", name="application.log")

In both cases you get back another rich object: an :class:`pylogbook.models.Attachment` instance.

..
    Another noteworthy method is :meth:`pylogbook.models.Event.delete`, which will remove the event from the
    logbook service (note that currently this is not fully implemented on the server side)::
        event.delete()

The Activity class
------------------

A :class:`pylogbook.models.Activity` instance can be attained through the
:meth:`pylogbook.Client.get_activities` method, which returns a dictionary of logbooks::

    all_activities = cli.get_activities()
    for activity in all_activities.values():
        print(activity.name, activity.activity_id)

Whilst a number of known activities have been explicitly named in :class:`pylogbook.NamedActivity`, it is
only through the :meth:`~pylogbook.Client.get_activities` method that you can get
access to *all* of the activities on the logbook server.

An :class:`~pylogbook.models.Activity` instance can be passed as well as / instead of a :class:`~pylogbook.NamedActivity`
wherever an activity is needed. For example::

    cli.add_event("A message based on one or more activity", activities=all_activities["LHC"])

