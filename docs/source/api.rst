.. _API_docs:

pylogbook API documentation
============================

.. rubric:: Modules

.. autosummary::
   :toctree: api

   .. Add the sub-packages that you wish to document below

   pylogbook
   pylogbook.auth
   pylogbook.exceptions
   pylogbook.models

