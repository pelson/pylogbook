# PyLogbook - interfacing with the eLogbook service

A python interface to the BE-OP Logbook API.

## Documentation

At its simplest level, posting to an eLogbook instance involves instantiating a Client, and posting an event to the client:

    import pylogbook
    cli = pylogbook.Client()
    event = cli.add_event("My event message", activities=pylogbook.NamedActivities.LHC)

There are a few things that you might choose to do from this point.
For full details of the API please see the documentation at
https://acc-py.web.cern.ch/gitlab/scripting-tools/pylogbook/.


## Interfacing with the legacy eLogbook interface

The legacy eLogbook interface is still in use at the time of writing (Jan 2021),
but the underlying REST interface that this package uses *does not support* these
legacy logbooks. Therefore, in order to interface with *both* the old and the new logbooks
in a far more constrained interface than is provided here, we recommend installing pylogbook v2:

```
pip install pylogbook==2.*
```

Documentation for this interface is available at https://acc-py.web.cern.ch/gitlab/scripting-tools/pylogbook/docs/v2.x/,
and the repository holds a branch for the v2.x series at https://gitlab.cern.ch/scripting-tools/pylogbook/-/tree/v2.x.

This legacy API will be maintained supported for as long as there exists legacy logbooks which need to be written to operationally.
If you have no need to access the legacy logbook service, please use the newer API (v3+).



